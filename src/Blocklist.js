const config = require('./data/client.json')
const request = require('request-promise').defaults({ pool: { maxSockets: Infinity } })

class Blocklist {

    #cache = [];
    #variant = "";

    constructor(variant, minutes) {
        this.getLatestBlocklist = this.getLatestBlocklist.bind(this);
        this.#variant = variant;
        this.getLatestBlocklist();
        setInterval(this.getLatestBlocklist, minutes * 60 * 1000)
    }

    async getLatestBlocklist() {
        const blocklistCheck = await request({
            uri: `https://www.pb-kronos.dev/${this.#variant}/blacklist`,
            json: true,
            simple: false,
            headers: {
              'Access-Key': config.kronosAPIKey,
            }
          });
        this.#cache = blocklistCheck;
    }

    isBlocked(userId) {
        const user = this.#cache.find((block) => block.id === userId);
        return user !== undefined;
    }
}

const PBSTBlocklist = new Blocklist("pbst", 60);
const TMSBlocklist = new Blocklist("tms", 60);

module.exports = {
    PBSTBlocklist, TMSBlocklist
}